
// Vincent Providence; vprovid1
// __Add your name and JHED above__
// ppm_io.c
// 601.220, Spring 2020
// Starter code for midterm project - feel free to edit/add to this file

#include <string.h>
#include <stdlib.h>
#include "ppm_io.h"

/* Read a PPM-formatted image from a file (assumes fp != NULL).
 * Returns the address of the heap-allocated Image struct it
 * creates and populates with the Image data.
 * Will additionally place error codes in the provided int given that 
 * it returns null.
 *
 * Error codes:
 * 1 - unexpected eof in header
 * 2 - pixel data amount is incorrect and less than expected value.
 * 3 - unexpected eof in data
 * 4 - a error flag was set by the reading of this file.
 * 5 - magic does not match P6, so this is not a PPM.
 * 6 - fp is null
 * 
 * 1, 2, 3, and 5 should trigger error code 3 for the final project.
 */
Image * read_ppm(FILE *fp, int *error_code) {

  if (!fp) {
    fprintf(stderr, "The supplied file pointer was null!\n");
    *error_code = 6;
    return NULL;
  }
  
  // width of image
  int w = 0;
  // height of image
  int h = 0;
  // max color value of image (0 - max short)
  int maxcolor = 0;
  /**
   * 0 is looking for p6
   * 1 is reading width and height
   * 2 is reading color max
   * 3 is finished 
   */
  int state = 0;
  // 1 if skipping a comment
  int skipcomment = 0;
  
  while (state != 3) {
    char ln[512];
    char c = fgetc(fp);
    if (c == '#') {
      skipcomment = 1;
    }
    ungetc(c, fp); //requires r+b fopen flag to use
    
    if (skipcomment > 0) {
      while (fgetc(fp) != '\n')
	continue;
      skipcomment = 0;
    } else {
      if (state == 0) {
         fscanf(fp, "%s", ln);
         if (strcmp(ln, "P6") != 0) {
	   fprintf(stderr, "Not a PPM file.\n");
	   *error_code = 5;
	   return NULL;
         }
         state = 1;
	 fgetc(fp); //clean out extra newline hanging in stream
      } else if (state == 1) {
         fscanf(fp, "%d %d", &h, &w);
         state = 2;
	 fgetc(fp); //clean out extra newline hanging in stream
      } else if (state == 2) {
	fscanf(fp, "%d", &maxcolor);
	state = 3;
	fgetc(fp); //clean out extra mewline hanging in stream
      }
    }    
    
    if (feof(fp)) {
      *error_code = 1;
      return NULL; //bad file
    }
  }
  Pixel *pixels = malloc(sizeof(Pixel) * w * h);
  int amt_pxls = fread(pixels, sizeof(Pixel), w * h, fp);
  if (amt_pxls < w*h) {
    fprintf(stderr, "Amount of pixels does not match expected value: %d vs %d\n", amt_pxls, w*h);
    *error_code = 2;
    return NULL;
  } 
  if (feof(fp)) {
    fprintf(stderr, "Unexpected EOF\n");
    *error_code = 3;
    return NULL;
  }
  if (ferror(fp)) { 
    fprintf(stderr, "Error reading image file\n");
    *error_code = 4;
    return NULL;
  }
  
  Image *image = malloc(sizeof(Image));
  image->rows = w;
  image->cols = h;
  image->data = pixels;
  
  return image;  
}


/* Write a PPM-formatted image to a file (assumes fp != NULL),
 * and return the number of pixels successfully written.
 */
int write_ppm(FILE *fp, const Image *im) {
  
  // check that fp is not NULL
  if (!fp) {
    fprintf(stderr, "The supplied file pointer was null!\n");
    return -1;
  }
  
  // write PPM file header, in the following format
  // P6
  // cols rows
  // 255
  fprintf(fp, "P6\n%d %d\n255\n", im->cols, im->rows);
  
  // write the pixel array
  int num_pixels_written = fwrite(im->data, sizeof(Pixel), im->cols * im->rows, fp);
  
  if (num_pixels_written != im->cols * im->rows) {
    fprintf(stderr, "Pixel data failed to write properly!\n");
  }
  
  return num_pixels_written;
}
