
// Vincent Providence; vprovid1
// __Add your name and JHED above__
// project.c
// 601.220, Spring 2020

#include "ppm_io.h"
#include "imageManip.h"

/*
 * Main is redirected to imageManip.
 */
int main(int argc, char* argv[]) {
  return imageManip(argc, argv);
}
