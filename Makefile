CC=gcc
CFLAGS=-std=c99 -pedantic -Wall -Wextra -g

project: ppm_io.o project.o imageManip.o
	$(CC) ppm_io.o project.o imageManip.o -lm -o project

project.o: project.c
	$(CC) $(CFLAGS) -c project.c

imageManip.o: imageManip.c imageManip.h
	$(CC) $(CFLAGS) -c imageManip.c

demo: demo_ppm.o ppm_io.o
	$(CC) demo_ppm.o ppm_io.o -o demo

demo_ppm.o: demo_ppm.c ppm_io.h
	$(CC) $(CFLAGS) -c demo_ppm.c

ppm_io.o: ppm_io.c ppm_io.h
	$(CC) $(CFLAGS) -c ppm_io.c

clean:
	rm -f *.o demo
