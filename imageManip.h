
// Vincent Providence; vprovid1
// __Add your name and JHED above__
// imageManip.h
// 601.220, Spring 2020
// Test read and write functions by storing and writing the image back to that filename.
// Sourced from the starter code from the class git repository.

int imageManip(int argc, char *argv[]);

/** 
 * Short for convert coordinates. Returns the position of a pixel at (c, r) in
 * an Image's data array.
 */
int cC(int r, int c, Image *im);

/**
 * Clamps a value between two numbers.
 */
int clamp(int i, int max, int min);

/**
 * Computes i^2 and returns it.
 */
float sq(float i);

/*
 * Returns 1 if this pixel is active in the image (i.e. not oob).
 */
int isPixelActive(int r, int c, Image *im);

/**
 * Fills a circle via distance formula, centered on (c, r). 
 * Assumes sz is less than sqrt(50).
 */
int fillCircle(int r, int c, Image *im, Image *ima, int sz);

/**
 * Returns the larger of a or b. If the two are equal, a is returned.
 */
int max(int a, int b);

/**
 * Looks at the position given and returns a value telling which image should be painted.
 * 0 - paint blend as normal
 * 1 - paint original input image
 * 2 - paint secondary input image
 * 3 - paint nothing, i.e. black pixels
 */
int imageToPaint(int r, int c, Image *im, Image *imex);

/**
 * Resizes an Image struct's data via malloc. Returns 0 if it worked.
 */
int resize(Image *ima, int rows, int cols);
