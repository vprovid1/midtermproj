
// Vincent Providence; vprovid1
// __Add your name and JHED above__
// imageManip.c
// 601.220, Spring 2020
// Test read and write functions by storing and writing the image back to that filename.
// Sourced from the starter code from the class git repository.

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <math.h>
#include "ppm_io.h"
#include "imageManip.h"

//define PI to 20 dec places
#define _PI_ 3.14159265358979323846

/**
 * The new main. It does everything!
 */
int imageManip(int argc, char *argv[]) {
   
  //param 0 = ./project
  //param 1 = src
  //param 2 = dst
  //param 3 = operation
  //param 4+ = params
  if (argc < 3) {
    fprintf(stderr, "No input or output file specified\n");
    return 1;
  }
  if (argc == 3) {
    fprintf(stderr, "No operation specified\n");
    return 4;
  }
   
  //load src file
  FILE *fpr = fopen(argv[1], "r+b");
  int error_code = 0;
  Image *im = read_ppm(fpr, &error_code); //im is old image
  if (!fpr) {
    if (error_code == 1 || error_code == 2 || error_code == 3 || error_code == 5) {
      if (error_code == 5)
         fprintf(stderr, "Input is not a PPM file.\n");
      else
         fprintf(stderr, "Specified input file is not a properly-formatted PPM file, or reading input somehow failed.\n");
      return 3;
    }
    fprintf(stderr, "Input file failed to open\n");
    return 2;
  }
  fclose(fpr);
  
  //setup new image arr for the output.
  Image *ima = malloc(sizeof(Image)); //ima is the output image
  if (!im) {
    fprintf(stderr, "Image allocation failed\n");
    return 8;
  }

  // specify dimensions for our Image
  ima->rows = im->rows;
  ima->cols = im->cols;
  
  // allocate space for array of Pixels
  Pixel *pix = malloc(sizeof(Pixel) * im->rows * im->cols);
  if (!pix) {
    fprintf(stderr, "Pixel array allocation failed\n");
    free(ima);
    free(im);
    return 8;
  }

  // let data field of Image point to the new array
  ima->data = pix;
  
  //operations for this image.
  if (strcmp(argv[3], "exposure") == 0) {
  
    //check number of parameters
    if (argc != 5) {
      fprintf(stderr, "Bad amount of parameters for exposure.\nUsage: ./project trees.ppm trees-exp-one.ppm exposure 1\n");
      free(ima->data);
      free(ima);
      free(im->data);
      free(im);
      return 5;
    }
    
    //calculate exponent
    float exponent = 0;
    sscanf(argv[4], "%f", &exponent);
    float multiplier = pow(2, exponent);
  
    //modify data
    for (int r = 0; r < im->rows; r++) {
      for (int c = 0; c < im->cols; c++) {
	int rv = im->data[cC(r, c, im)].r;
	int gv = im->data[cC(r, c, im)].g;
	int bv = im->data[cC(r, c, im)].b;
	rv = clamp(rv * multiplier, 255, 0);
	gv = clamp(gv * multiplier, 255, 0);
	bv = clamp(bv * multiplier, 255, 0);
	Pixel pxneo = {rv, gv, bv};
	ima->data[cC(r, c, im)] = pxneo;
      }
    }  
  } else if (strcmp(argv[3], "blend") == 0) {
  
    //check number of parameters
    if (argc != 6) {
      fprintf(stderr, "Bad amount of parameters for blend.\nUsage: ./project trees.ppm trees-building-blended.ppm blend building.ppm 0.5\n");
      free(ima->data);
      free(ima);
      free(im->data);
      free(im);
      return 7;
    }
    
    //read alpha value
    float alpha = 0;
    sscanf(argv[5], "%f", &alpha);

    //get rid of bad alpha
    if (alpha < 0 || alpha > 1) {
      fprintf(stderr, "Alpha must be between range [0-1]\n");
      free(ima->data);
      free(ima);
      free(im->data);
      free(im);
      return 6;
    }
    
    //open secondary image and store in image struct
    FILE *fpralt = fopen(argv[4], "r+b");
    int error_code_alt = 0;
    Image *imex = read_ppm(fpralt, &error_code_alt); //imex is secondary blend source image
    if (!fpralt) {
      if (error_code_alt == 1 || error_code_alt == 2 || error_code_alt == 3 || error_code_alt == 5) {
        if (error_code_alt == 5)
          fprintf(stderr, "Secondary input is not a PPM file.\n");
        else
          fprintf(stderr, "Secondary input file is not a properly-formatted PPM file, or reading input somehow failed.\n");
        free(ima->data);
        free(ima);
        free(im->data);
        free(im);
        return 3;
      }
      fprintf(stderr, "Secondary input file failed to open\n");
      free(ima->data);
      free(ima);
      free(im->data);
      free(im);
      return 2;
    }
    fclose(fpralt);

    //resize ima so that it can store all both images
    if (resize(ima, max(imex->rows, im->rows), max(imex->cols, im->cols)) != 0) {
      fprintf(stderr, "Pixel array allocation failed\n");
      free(im->data);
      free(im);
      return 8;
    }
    
    //modify image data
    for (int r = 0; r < ima->rows; r++) {
      for (int c = 0; c < ima->cols; c++) {
      
      // determine the best way to paint this pixel based on position
      // and then set rvf, gvf, and bvf as necessary
	int rvf = 0;
	int gvf = 0;
	int bvf = 0;
	switch (imageToPaint(r, c, im, imex)) {
	case 0: ; //semicolon is required here
   
     //blend both images
	  int rv = im->data[cC(r, c, im)].r;
	  int gv = im->data[cC(r, c, im)].g;
	  int bv = im->data[cC(r, c, im)].b;
	  int rva = imex->data[cC(r, c, imex)].r;
	  int gva = imex->data[cC(r, c, imex)].g;
	  int bva = imex->data[cC(r, c, imex)].b;
          
	  rvf = clamp((int) (alpha * rv + (1-alpha) * rva), 255, 0);
	  gvf = clamp((int) (alpha * gv + (1-alpha) * gva), 255, 0);
	  bvf = clamp((int) (alpha * bv + (1-alpha) * bva), 255, 0);
	  break;
	case 1:
   
     //only paint the base image
	  rvf = im->data[cC(r, c, im)].r;
	  gvf = im->data[cC(r, c, im)].g;
	  bvf = im->data[cC(r, c, im)].b;
	  break;
	case 2:
   
     //only paint the secondary image
	  rvf = imex->data[cC(r, c, imex)].r;
	  gvf = imex->data[cC(r, c, imex)].g;
	  bvf = imex->data[cC(r, c, imex)].b;
	  break;
     
	case 3: //paint black
	default: //fall through: paint black
	  break;
	}
	Pixel pxneo = {rvf, gvf, bvf};
	ima->data[cC(r, c, ima)] = pxneo;
      }
    }
    
  } else if (strcmp(argv[3], "zoom_in") == 0) {
  
    //check number of parameters
    if (argc != 4) {
      fprintf(stderr, "Bad amount of parameters for zoom in.\nUsage: ./project buidling.ppm building-zoomed-in.ppm zoom_in\n");
      free(ima->data);
      free(ima);
      free(im->data);
      free(im);
      return 7;
    }

    //resize image to accomodate double size
    if (resize(ima, im->rows*2, im->cols*2) != 0) {
      fprintf(stderr, "Pixel array allocation failed\n");
      free(im->data);
      free(im);
      return 8;
    }
    
    //paint each pixel as 2x2
    for (int r = 0; r < im->rows; r++) {
      for (int c = 0; c < im->cols; c++) {
	int rv = im->data[cC(r, c, im)].r;
	int gv = im->data[cC(r, c, im)].g;
	int bv = im->data[cC(r, c, im)].b;
	Pixel pxneo = {rv, gv, bv};
	ima->data[cC(r*2, c*2, ima)] = pxneo;
	ima->data[cC(r*2+1, c*2, ima)] = pxneo;
	ima->data[cC(r*2, c*2+1, ima)] = pxneo;
	ima->data[cC(r*2+1, c*2+1, ima)] = pxneo;
      }
    }
    
    
  } else if (strcmp(argv[3], "zoom_out") == 0) {
  
    //check number of parameters
    if (argc != 4) {
      fprintf(stderr, "Bad amount of parameters for zoom out.\nUsage: ./project trees.ppm trees-zoomed-out.ppm zoom_out\n");
      free(ima->data);
      free(ima);
      free(im->data);
      free(im);
      return 7;
    }
    
    //resize image so extra memory is cut off.
    if (resize(ima, im->rows/2, im->cols/2) != 0) {
      fprintf(stderr, "Pixel array allocation failed\n");
      free(im->data);
      free(im);
      return 8;
    }
    
    //downsize each 2x2 square to one
    for (int r = 0; r < im->rows; r+=2) {
      for (int c = 0; c < im->cols; c+=2) {
	int rv = im->data[cC(r, c, im)].r;
	int gv = im->data[cC(r, c, im)].g;
	int bv = im->data[cC(r, c, im)].b;
	int rv2 = 0, gv2 = 0, bv2 = 0, rv3 = 0, gv3 = 0, bv3 = 0, rv4 = 0, gv4 = 0, bv4 = 0;
	float countpx = 1;
   
   // for each pixel that is not the main, add 1 to the number of pixels counted
   // and add set it to the respective value.
	if (isPixelActive(r+1, c, im)) {
	  rv2 = im->data[cC(r+1, c, im)].r;
	  gv2 = im->data[cC(r+1, c, im)].g;
	  bv2 = im->data[cC(r+1, c, im)].b;
	  countpx++;
	}
	if (isPixelActive(r, c+1, im)) {
	  rv3 = im->data[cC(r, c+1, im)].r;
	  gv3 = im->data[cC(r, c+1, im)].g;
	  bv3 = im->data[cC(r, c+1, im)].b;
	  countpx++;
	}
	if (isPixelActive(r+1, c+1, im)) {
	  rv4 = im->data[cC(r+1, c+1, im)].r;
	  gv4 = im->data[cC(r+1, c+1, im)].g;
	  bv4 = im->data[cC(r+1, c+1, im)].b;
	  countpx++;
	}
	
   //average out the pixel colors and set
	Pixel pxneo = {(int) ((rv+rv2+rv3+rv4)/countpx), (int) ((gv+gv2+gv3+gv4)/countpx), (int) ((bv+bv2+bv3+bv4)/countpx)};
	ima->data[cC(r/2, c/2, ima)] = pxneo;
      }
    }
  } else if (strcmp(argv[3], "pointilism") == 0) {
  
    //check number of parameters
    if (argc != 4) {
      fprintf(stderr, "Bad amount of parameters for pointilism.\nUsage: ./project trees.ppm trees-pointilism.ppm pointilism\n");
      free(ima->data);
      free(ima);
      free(im->data);
      free(im);
      return 7;
    }
    
    //copy paste im to ima, as this operation operates over itself
    for (int r = 0; r < im->rows; r++) {
      for (int c = 0; c < im->cols; c++) {
	int rv = im->data[cC(r, c, im)].r;
	int gv = im->data[cC(r, c, im)].g;
	int bv = im->data[cC(r, c, im)].b;
	Pixel pxneo = {rv, gv, bv};
	ima->data[cC(r, c, im)] = pxneo;
      }
    }
    
    
    // specifically made to generate the same result as the examples. order cannot be modified
    // by far hardest part of proj >:(
    for (int i = (int) ((im->rows*im->cols)*.03f); i>=0; i--) {
      int c = rand()%im->cols;
      int r = rand()%im->rows;
      fillCircle(r, c, im, ima, rand()%5 + 1);
    }

  } else if (strcmp(argv[3], "swirl") == 0) {
  
    //check number of parameters
    if (argc != 7) {
      fprintf(stderr, "Bad amount of parameters for swirl.\nUsage: ./project trees.ppm trees-swirl-100.ppm swirl 400 300 100\n");
      free(ima->data);
      free(ima);
      free(im->data);
      free(im);
      return 7;
    }
    
    //initialize variables for calculations
    int cx = 0;
    int cy = 0;
    int sca = 0;
    sscanf(argv[4], "%d", &cx);
    sscanf(argv[5], "%d", &cy);
    sscanf(argv[6], "%d", &sca);

    if (sca < 0) {
      fprintf(stderr, "Scale must be > 0.\n");
      free(ima->data);
      free(ima);
      free(im->data);
      free(im);
      return 6;
    }
    
    for (int r = 0; r < im->rows; r++) {
      for (int c = 0; c < im->cols; c++) {
        //place a black pixel to prevent junk data in final
        Pixel pxbg = {0, 0, 0};
	ima->data[cC(r, c, im)] = pxbg;
   
        //get the angle and position of the pixel to take color from
        float alpha = sqrt(sq(r - cy) + sq(c - cx))/(float) sca;
        int newr = cy + (r - cy)*cos(alpha) + (c - cx)*sin(alpha);
	int newc = cx - (r - cy)*sin(alpha) + (c - cx)*cos(alpha);
        
        if (newr < 0 || newc < 0 || newr >= im->rows || newc >= im->cols) {
          //whoops! this pixel is oob
        } else {
        //set pixel
	  int rv = im->data[cC(newr, newc, im)].r;
	  int gv = im->data[cC(newr, newc, im)].g;
	  int bv = im->data[cC(newr, newc, im)].b;
	  Pixel pxneo = {rv, gv, bv};
	  ima->data[cC(r, c, im)] = pxneo;
        }
      }
    }
  } else if (strcmp(argv[3], "blur") == 0) {
  
    //check amount of parameters
    if (argc != 5) {
      fprintf(stderr, "Bad amount of parameters for blur.\nUsage: ./project trees.ppm trees-exp-one.ppm blur 1\n");
      free(ima->data);
      free(ima);
      free(im->data);
      free(im);
      return 5;
    }
    
    //store "sigma"
    float sigma = 0;
    sscanf(argv[4], "%f", &sigma);
    if (sigma <= 0) {
      fprintf(stderr, "Blur intensity must be > 0.\n");
      free(ima->data);
      free(ima);
      free(im->data);
      free(im);
      return 6;
    }
    
    //generate matrices
    int size = sigma*10;
    if (size%2 == 0) //size of array must be odd
      size++;
    float matrix[size*size];
    float matrixtempr[size*size];
    float matrixtempg[size*size];
    float matrixtempb[size*size];
    
    //apply gaussian formula to the matrix
    int dx = size/2;
    int dy = size/2;
    for (int i = 0; i < size; i++) {
      for (int j = 0; j < size; j++) {
	matrix[i*size+j] = (1.0f / (2.0f * _PI_ * sq(sigma))) * exp( -(sq(dx-i) + sq(dy-j)) / (2.0f * sq(sigma)));
	//printf("%0.5f ", matrix[i*size+j]); print matrix
      }
      //printf("\n"); print matrix
    }
    
    //convolve pixels
    for (int r = 0; r < im->rows; r++) {
      for (int c = 0; c < im->cols; c++) {
	int rv = im->data[cC(r, c, im)].r;
	int gv = im->data[cC(r, c, im)].g;
	int bv = im->data[cC(r, c, im)].b;
	float rtotal = 0;
	float gtotal = 0;
	float btotal = 0;
	float msum = 0;
	for (int i = 0; i < size; i++) {
	  for (int j = 0; j < size; j++) {
	    if (isPixelActive(r - dx + i, c - dy + j, im)) {
	      int rvt = im->data[cC(r - dx + i, c - dy + j, im)].r;
	      int gvt = im->data[cC(r - dx + i, c - dy + j, im)].g;
	      int bvt = im->data[cC(r - dx + i, c - dy + j, im)].b;
	      
	      matrixtempr[i*size+j] = matrix[i*size+j] * rvt;
	      rtotal+=matrixtempr[i*size+j];
	      matrixtempg[i*size+j] = matrix[i*size+j] * gvt;
	      gtotal+=matrixtempg[i*size+j];
	      matrixtempb[i*size+j] = matrix[i*size+j] * bvt;
	      btotal+=matrixtempb[i*size+j];
	      
	      msum += matrix[i*size+j];
	    }
	  }
	}
	rv = clamp((int) (rtotal/msum), 255, 0);
	gv = clamp((int) (gtotal/msum), 255, 0);
	bv = clamp((int) (btotal/msum), 255, 0);
	Pixel pxneo = {rv, gv, bv};
	ima->data[cC(r, c, im)] = pxneo;
      }
    }
  } else {
    //none of the other operations were launched
    fprintf(stderr, "Invalid operation\n");
    free(ima->data);
    free(ima);
    free(im->data);
    free(im);
    return 4;
  }
  
  // write image to disk
  FILE *fp = fopen(argv[2], "wb");
  write_ppm(fp, ima);
  
  if (!fp) {
    fprintf(stderr, "Output file failed to write\n");
    free(ima->data);
    free(ima);
    free(im->data);
    free(im);
    return 7;
  }
  
  //free and end successfully :)
  fclose(fp);
  free(ima->data);
  free(ima);
  free(im->data);
  free(im);
  return 0;
}

/** 
 * Short for convert coordinates. Returns the position of a pixel at (c, r) in
 * an Image's data array.
 */
int cC(int r, int c, Image *im) {
  return (r * im->cols) + c;
}

/*
 * Returns 1 if this pixel is active in the image (i.e. not oob).
 */
int isPixelActive(int r, int c, Image *im) {
  int cc = cC(r, c, im);
  if (r < 0)
    return 0;
  if (c < 0)
     return 0;
  if (r >= im->rows)
    return 0;
  if (c >= im->cols)
    return 0;
  return (cc >= 0 && cc < (im->cols*im->rows));
}

/**
 * Clamps a value between two numbers.
 */
int clamp(int i, int max, int min) {
   if (i > max)
     i = max;
   if (i < min)
     i = min;
   return i;
}

/**
 * Computes i^2 and returns it.
 */
float sq(float i) {
  return i*i;
}

/**
 * Returns the larger of a or b. If the two are equal, a is returned.
 */
int max(int a, int b) {
   return a>b?a:b;
}

/**
 * Looks at the position given and returns a value telling which image should be painted.
 * 0 - paint blend as normal
 * 1 - paint original input image
 * 2 - paint secondary input image
 * 3 - paint nothing, i.e. black pixels
 */
int imageToPaint(int r, int c, Image *im, Image *imex) {
  int onIm = isPixelActive(r, c, im);
  int onImEX = isPixelActive(r, c, imex);
  if (onIm && !onImEX)
    return 1;
  if (!onIm && onImEX)
    return 2;
  if (onIm && onImEX)
    return 0;
  return 3;
}

/**
 * Fills a circle via distance formula, centered on (c, r). 
 * Assumes sz is less than sqrt(50).
 */
int fillCircle(int r, int c, Image *im, Image *ima, int sz) {
   int rv = ima->data[cC(r, c, im)].r;
   int gv = ima->data[cC(r, c, im)].g;
   int bv = ima->data[cC(r, c, im)].b;
   Pixel fill = {rv, gv, bv};
   for (int i = 0; i <= 10; i++) { //sz can't be bigger than 10 anyway
      for (int j = 0; j <= 10; j++) {
         int itposx = r - 5 + i;
         int itposy = c - 5 + j;
         float dist = sqrt(sq(itposx - r) + sq(itposy - c));
         if (dist <= sz && isPixelActive(itposx, itposy, im)) {
            ima->data[cC(itposx, itposy, im)] = fill;
         }
      }
   }
   return 0;
}

/**
 * Resizes an Image struct's data via malloc. Returns 0 if it worked.
 */
int resize(Image* ima, int rows, int cols) {
  
  //free old data
  free(ima->data);
  
  //resize and reallocate
  ima->rows = rows;
  ima->cols = cols;
  Pixel *pix = malloc(sizeof(Pixel) * ima->rows * ima->cols);
  if (!pix) {
    fprintf(stderr, "Pixel array allocation failed\n");
    free(ima);
    return 1;
  }
  
  //return
  ima->data = pix;
  return 0;
}
